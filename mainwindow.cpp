#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <Windowsx.h>
#include <dwmapi.h>
#pragma comment(lib, "dwmapi.lib")
#pragma comment(lib, "user32.lib")

#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
   QMainWindow(parent),
   ui(new Ui::MainWindow),
   fixFrameless( false )
{
   ui->setupUi(this);

   this->setWindowFlags( Qt::FramelessWindowHint  );

   QWidget *w = new QWidget();
   w->setAutoFillBackground( true );
   w->setStyleSheet( "background: red" );
   this->setCentralWidget( w );
}

MainWindow::~MainWindow()
{
   delete ui;
}

void MainWindow::setTitleHeight(int value)
{
    if (value != m_titleHeight)
    {
        m_titleHeight = value;
        emit titleHeightChanged();
    }
}

int MainWindow::titleHeight() const
{
    return m_titleHeight;
}

struct HitRegion
{
    RECT    bounds;
    LRESULT region;

    bool contains(const POINT& point) const
    {
        return
            point.x >= bounds.left && point.x < bounds.right &&
            point.y >= bounds.top  && point.y < bounds.bottom;
    }
};

LRESULT MainWindow::hit_test(POINT point) const
{
    const auto border_x = ::GetSystemMetrics(SM_CYFRAME) + ::GetSystemMetrics(SM_CXPADDEDBORDER);
    const auto border_y = ::GetSystemMetrics(SM_CYFRAME) + ::GetSystemMetrics(SM_CXPADDEDBORDER);

    RECT winrect;
    ::GetWindowRect((HWND)this->winId(), &winrect);

    const HitRegion hitregions[]
    {
        {
            { winrect.left, winrect.bottom - border_y, winrect.left + border_x, winrect.bottom }, HTBOTTOMLEFT
        },
        {
            { winrect.right - border_x, winrect.bottom - border_y, winrect.right, winrect.bottom }, HTBOTTOMRIGHT
        },
        {
            { winrect.left, winrect.top, winrect.left + border_x  , winrect.top + border_y }, HTTOPLEFT
        },
        {
            { winrect.right - border_x , winrect.top, winrect.right, winrect.top + border_y }, HTTOPRIGHT
        },
        {
            { winrect.left, winrect.top, winrect.left + border_x , winrect.bottom }, HTLEFT
        },
        {
            { winrect.right - border_x , winrect.top, winrect.right, winrect.bottom }, HTRIGHT
        },
        {
            { winrect.left, winrect.top, winrect.right, winrect.top + border_y }, HTTOP
        },
        {
            { winrect.left, winrect.bottom - border_y, winrect.right, winrect.bottom }, HTBOTTOM
        },
        {
            { winrect.left, winrect.top, winrect.right, winrect.top + this->titleHeight() }, HTCAPTION
        },
        {
            { winrect.left, winrect.top + this->titleHeight(), winrect.right, winrect.bottom }, HTCLIENT
        }
    };

    for (auto &&hr : hitregions)
    {
        if (hr.contains(point))
        {
            return hr.region;
        }
    }

    return HTNOWHERE;
}

bool MainWindow::composition_enabled() const
{
    BOOL composition_enabled = FALSE;
    bool success = ::DwmIsCompositionEnabled(&composition_enabled) == S_OK;
    return composition_enabled && success;
}

bool MainWindow::nativeEvent(const QByteArray &eventType, void *message, long *result)
{
    MSG *msg = static_cast<MSG *>(message);

    switch (msg->message)
    {
        case WM_SHOWWINDOW:
            {
                if (this->composition_enabled())
                {
                    static const MARGINS shadow_state{ 1, 1, 1, 1 };
                    ::DwmExtendFrameIntoClientArea((HWND)this->winId(), &shadow_state);
                }

                // Do not return true: Let Qt handle this event.
                // We just needed a hook to enable shadows.
                return false;
            }

        case WM_NCCALCSIZE:
            {
               if( !fixFrameless ) {
                  fixFrameless = true;
                  LONG style = GetWindowLong( (HWND)this->winId(), GWL_STYLE );
                  style |= WS_POPUP
                        | WS_TABSTOP
                        | WS_GROUP
                        | WS_THICKFRAME
                        | WS_SYSMENU
                        | WS_DLGFRAME
                        | WS_BORDER
                        | WS_CAPTION
                        | WS_CLIPCHILDREN
                        | WS_CLIPSIBLINGS
                        | WS_VISIBLE;

                  SetWindowLong((HWND)this->winId(), GWL_STYLE, style);
                  SetWindowPos((HWND)this->winId(), NULL, 0, 0, 0, 0,
                               SWP_FRAMECHANGED | SWP_NOMOVE | SWP_NOSIZE |
                               SWP_NOZORDER | SWP_NOACTIVATE | SWP_NOREDRAW);
               }

                // This is what kills the border and extends the client rect to the size of the window rect.
                *result = 0;
                return true;
            }

        case WM_NCHITTEST:
            {
                // When we have no border or title bar, we need to perform our
                // own hit testing to allow resizing and moving.
                const POINT cursor{
                    GET_X_LPARAM(msg->lParam),
                    GET_Y_LPARAM(msg->lParam)
                };

                *result = this->hit_test(cursor);
                return true;
            }

        case WM_NCACTIVATE:
            {
                if (!this->composition_enabled())
                {
                    // Prevents window frame reappearing on window activation in "basic" theme,
                    // where no aero shadow is present.
                    *result = 1;
                    return true;
                }
                else
                {
                    return false;
                }
            }
//       case WM_SIZE:
//       {
//           RECT winrect;
//           ::GetWindowRect((HWND)this->winId(), &winrect);

//           WINDOWPLACEMENT wp;
//           wp.length = sizeof(WINDOWPLACEMENT);
//           GetWindowPlacement((HWND)this->winId(), &wp);
//           if (wp.showCmd == SW_MAXIMIZE)
//           {
//                this->setGeometry(8, 8 //Maximized window draw 8 pixels off screen
//                    , winrect.right - 16
//                    , winrect.bottom - 16);
//            }
//            else
//            {
//                this->setGeometry( winrect.left, winrect.top, winrect.right, winrect.bottom  );
//            }
//           return true;
//       }
    }

    qDebug() << this->geometry() << this->frameGeometry();

    return QMainWindow::nativeEvent(eventType, message, result);
}
