****FramlessSnappingMainWindow

Snapping Borderless Window, based on sample of BodrelessWindow (https://github.com/deimos1877/BorderlessWindow).
The main advantage is: not using WinExtras or QWinHost or something, just Qt with native WinAPI calls.

Modifications about removing a title was taken from some forum post, i do not remember where, but thanks to the respective author.