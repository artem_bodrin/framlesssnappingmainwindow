#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <windows.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
   Q_OBJECT

public:
   explicit MainWindow(QWidget *parent = 0);
   ~MainWindow();

   bool nativeEvent(const QByteArray &eventType, void *message, long *result) override;

   void setTitleHeight(int value);
   int titleHeight() const;

signals:
   void titleHeightChanged();

private:
   Ui::MainWindow *ui;

   LRESULT hit_test(POINT point) const;
   bool composition_enabled() const;

   int m_titleHeight = 20;

   bool fixFrameless;
};

#endif // MAINWINDOW_H
